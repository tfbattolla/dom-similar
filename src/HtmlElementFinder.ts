
import { JSDOM } from 'jsdom'
import fs from 'fs'
import cssPath from 'css-path'
import ElementFactory from './factory/ElementFactory';


/**
 * Html element finder
 */
export default class HtmlElementFinder {
    targetHtmlPath: string
    sampleHtmlPath: string

    constructor(targetHtmlPath: string, sampleHtmlPath: string) {
        this.targetHtmlPath = targetHtmlPath
        this.sampleHtmlPath = sampleHtmlPath
    }

    
    /**
     * Gets document using JSDOM
     * @param filePath 
     * @returns  
     */
    static getDocument(filePath: string) {
        const file = fs.readFileSync(filePath);
        const jsdom = new JSDOM(file)
        return jsdom.window.document
    }


    /**
     * Find the element that matchs more accurately the element of the target file
     * @param targetSelector 
     * @returns {
     *  element: Element,
     *  score: number
     * }
     */
    findBestElementMatch(targetSelector: string): {element: Element, score: number}{
        const targetRawElement = HtmlElementFinder.getDocument(this.targetHtmlPath).querySelector(targetSelector)
        const targetElement = ElementFactory.getComparableElement(targetRawElement)

        if(targetElement){
            const sampleDom = HtmlElementFinder.getDocument(this.sampleHtmlPath)
            const allElements = sampleDom.querySelectorAll('*')
    
            let bestMatch : Element
            let biggestNumber = 0
            allElements.forEach((el: Element) => {
                const num = targetElement.compareElement(el)
                if (num > biggestNumber) {
                    bestMatch = el
                    biggestNumber = num
                } 
            })
            return {element: bestMatch, score: biggestNumber}
        }

        throw new Error(`Element not found with selector: ${targetSelector}`)
    }


    /**
     * Find the path of the element that matchs more accurately the element of the target file
     * @param targetSelector 
     * @returns {
        *  element: Element,
        *  score: number
        * }
        */
    findBestElementMatchPath(targetSelector: string) : {path: string, score: number} {
        const {element, score} = this.findBestElementMatch(targetSelector)
        return {path: cssPath(element), score: score}
    }
}