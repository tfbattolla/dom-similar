

/**
 * Gets functions from js string
 * @param jsString: string 
 * @returns string[] 
 */
export function getFunctionsFromJsString(jsString: string): string[] {
    return jsString.match(/([. ]*[\w\d]*?)\(/ig)
}