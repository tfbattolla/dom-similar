import { SingleStringAttribute } from './Attribute'


export default class RefAttribute extends SingleStringAttribute {
    constructor(data: string, weight: number = 100) {
        super('ref', data, weight)
    }
}