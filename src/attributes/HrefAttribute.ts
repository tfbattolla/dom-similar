import { SingleStringAttribute } from './Attribute'


export default class HrefAttribute extends SingleStringAttribute {
    constructor(data: string, weight: number = 1000) {
        super('href', data, weight)
    }
}