import { StringAttribute } from './Attribute'


export default class TextContentAttribute extends StringAttribute<Set<string>> {
    constructor (data: string, weight: number = 10) {
        super('text', data, weight)
    }
    getComparable(data: string) : Set<string>{
        return new Set(data.split(' '))
    }

    getSimilarity(textAttribute: TextContentAttribute) : number{
        let coincidence = 0
        textAttribute.comparable.forEach((c: string) => {
            if(this.comparable.has(c)) {
                coincidence++
            }
        })
        return coincidence * this.weigth
    }
}