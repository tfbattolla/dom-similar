import { StringAttribute } from './Attribute'


export default class ClassAttribute extends StringAttribute<Set<string>> {
    constructor (data: string, weight: number = 10) {
        super('class', data, weight)
    }
    getComparable(data: string) : Set<string>{
        return new Set(data.split(' '))
    }

    getSimilarity(classAttribute: ClassAttribute) : number{
        let coincidence = 0
        classAttribute.comparable.forEach((c: string) => {
            if(this.comparable.has(c)) {
                coincidence++
            }
        })
        return coincidence * this.weigth
    }
}