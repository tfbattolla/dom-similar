import {SingleStringAttribute } from './Attribute'


export default class IdAttribute extends SingleStringAttribute {
    constructor(data: string, weight: number = 1000) {
        super('id', data, weight)
    }
}