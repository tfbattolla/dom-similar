export abstract class Attribute<T, C> {
    name: string
    data: T
    weigth: number
    comparable: C

    constructor(name: string, data: T, weigth: number) {
        this.name = name
        this.data = data
        this.weigth = weigth
        this.comparable = this.getComparable(this.data)
    }

    abstract getComparable(data: T): C
    abstract getSimilarity(attribute: Attribute<T, C>): number
}

export abstract class StringAttribute<C> extends Attribute<string, C> {
    constructor(name: string, data: string, weight: number = 1) {
        super(name, data, weight)
    }

    abstract getComparable(data: String): C

    abstract getSimilarity(attribute: StringAttribute<C>): number 
}


export class SingleStringAttribute extends StringAttribute<string> {
    constructor(name: string, data: string, weight: number = 1) {
        super(name, data, weight)
    }

    getComparable(data: string): string {
        return data.trim()
    }

    getSimilarity(attribute: SingleStringAttribute): number {
        return attribute.comparable === this.comparable ? this.weigth : 0
    }
}