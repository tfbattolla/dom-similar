import { StringAttribute } from './Attribute'
import { getFunctionsFromJsString } from '../utils/getFunctionsFromJsString'


export default class JsAttribute extends StringAttribute<Set<string>> {
    constructor (name: string ,data: string, weight: number = 100) {
        super(name, data, weight)
    }
    getComparable(data: string) : Set<string>{
        const functions = getFunctionsFromJsString(data)
        return new Set(functions)
    }

    getSimilarity(classAttribute: JsAttribute) : number{
        let coincidence = 0
        classAttribute.comparable.forEach((c: string) => {
            if(this.comparable.has(c)) {
                coincidence++
            }
        })
        return coincidence * this.weigth
    }
}