import { StringAttribute } from './Attribute'
import { getFunctionsFromJsString } from '../utils/getFunctionsFromJsString'


export default class CssPathAttribute extends StringAttribute<string[]> {
    constructor (data: string, weight: number = 100) {
        super('cssPath', data, weight)
    }
    
    getComparable(data: string) : string[]{
        const path = data.split('>')
        return path
    }

    getSimilarity(cssPathAttribute: CssPathAttribute) : number{
        let coincidence = 0
        const pathLength = this.comparable.length
        for (let index = 0; index < pathLength; index++) {
            const element = this.comparable[index]
            const otherElement = cssPathAttribute.comparable[index]
            if(element && otherElement && (element === otherElement)) {
                coincidence++
            } else {
                break
            }
        }
        return coincidence * this.weigth
    }
}