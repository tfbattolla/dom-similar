import { SingleStringAttribute } from './Attribute'


export default class TitleAttribute extends SingleStringAttribute {
    constructor(data: string, weight: number = 1000) {
        super('title', data, weight)
    }
}