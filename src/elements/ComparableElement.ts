import { StringAttribute } from '../attributes/Attribute'
import AttributeFactory from '../factory/AttributeFactory'
import cssPath from 'css-path'


export type Weight = 0 | 1 | 2| 3 | 5 | 8 | 13 | 21 | 34 | 55 | 89 | 144 | 233 | 377


/**
 * Html Element with attributes to compare with other Comparable Elements. Specifying the weight of each attribute. 
 */
export default class ComparableElement {
    attributes: {
        [name: string]: StringAttribute<string | string [] | Set<string>>
    }
    static weights: {[name: string]: Weight} = {
        id: 377,
        'class': 1,
        'cssPath': 1,
        'title': 1,
        'href': 1,
        'jsFunctions': 1,
        'content': 1,
        'ref': 1
    }
    
    constructor (attributes) {
        this.attributes = attributes
    }


    /**
     * Compares with an element getting the similarity score
     * @param element 
     * @returns score: number 
     */
    compareElement(element: Element): number {
        const compareElement = ComparableElement.fromRawElement(element)
        return Object.keys(this.attributes).reduce((acc, name)=> {
            const attribute = this.attributes[name]
            const compareAttribute = compareElement.attributes[name]
            if(attribute && compareAttribute) {
                return acc + attribute.getSimilarity(compareAttribute)
            }
            return acc
        }, 0)
    }
 

    /**
     * Generate Comparable Element from an Html Element
     * @param element 
     * @returns ComparableElement 
     */
    static fromRawElement(element: Element) : ComparableElement {
        const attributesName = element.getAttributeNames()
        const attributes = attributesName.reduce((acc, name)=> {
            acc[name] = AttributeFactory.getAttribute(name, element.getAttribute(name), this.weights[name])
            return acc
        }, {})
        attributes['cssPath'] = AttributeFactory.getAttribute('cssPath', cssPath(element), this.weights['cssPath'])
        attributes['content'] = AttributeFactory.getAttribute('cssPath', element.textContent, this.weights['content'])
        return new ComparableElement(attributes)
    }
}