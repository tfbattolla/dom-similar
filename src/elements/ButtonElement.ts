import ComparableElement, { Weight } from './ComparableElement'

export default class ButtonElement  extends ComparableElement {
    static weights: {[name: string]: Weight} = {
        id: 377,
        'class': 144,
        'cssPath': 55,
        'title': 34,
        'href': 34,
        'jsFunctions': 21,
        'content': 144,
        'ref': 5
    }
}