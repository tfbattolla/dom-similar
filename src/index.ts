
import HtmlElementFinder from './HtmlElementFinder';

const args = process.argv.slice(2)
const target = args[0] || './samples/sample-0/sample-0.html'
const sample = args[1] || './samples/sample-3/sample-3.html'
const selector = args[2] || '#make-everything-ok-button'
const htmlFinder = new HtmlElementFinder(target, sample)
const matchElement = htmlFinder.findBestElementMatchPath(selector)
console.log(matchElement.path)        


