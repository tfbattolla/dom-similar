import ButtonElement from "../elements/ButtonElement";
import ComparableElement from "../elements/ComparableElement";

export default class ElementFactory {
    static getComparableElement(rawElement: Element) {
        switch (rawElement.tagName) {
            case 'A': 
                return ButtonElement.fromRawElement(rawElement)
            case 'BUTTON':
                return ButtonElement.fromRawElement(rawElement)
            default: 
                return ComparableElement.fromRawElement(rawElement)
        }
    }
}