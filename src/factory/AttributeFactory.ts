import { StringAttribute, SingleStringAttribute } from "../attributes/Attribute";
import IdAttribute from "../attributes/IdAttribute";
import ClassAttribute from "../attributes/ClassAttribute";
import HrefAttribute from "../attributes/HrefAttribute";
import TextContentAttribute from "../attributes/TextContentAttribute";
import JsAttribute from "../attributes/JsAttribute";
import TitleAttribute from "../attributes/TitleAttribute";
import CssPathAttribute from "../attributes/CssPathAttribute";
import RefAttribute from "../attributes/RefAttribute";


export default class AttributeFactory {
    static getAttribute(name: string, data: string, weight: number) : StringAttribute<string | string[] | Set<string>> {
        if(name === 'id') {
            return new IdAttribute(data, weight)
        } else if (name === 'class') {
            return new ClassAttribute(data, weight)
        } else if (name === 'href') {
            return new HrefAttribute(data, weight)
        } else if (name === 'ref') {
            return new RefAttribute(data, weight)
        } else if (name === 'content') {
            return new TextContentAttribute(data, weight)
        } else if (name.startsWith('on')) {
            return new JsAttribute(name, data, weight)
        } else if (name === 'title') {
            return new TitleAttribute(data, weight)
        } else if (name === 'cssPath') {
            return new CssPathAttribute(data, weight)
        }
        return new SingleStringAttribute(name, data, weight | 1)
    }
}