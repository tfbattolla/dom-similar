# HtmlElementFinder

## Get Started

1. Install dependencies
`npm i`

2. Compile Typescript code
`npm run compile`

## Find element

`node bundle.js <input_origin_file_path> <input_other_sample_file_path> <selector>`

If `<selector>` is not specified default value is "#make-everything-ok-button"

## Binary version

### For MacOS
`dom-similar-macos <input_origin_file_path> <input_other_sample_file_path> <selector>`

### For Linux
`dom-similar-linux <input_origin_file_path> <input_other_sample_file_path> <selector>`

### For Windows
`dom-similar-windows <input_origin_file_path> <input_other_sample_file_path> <selector>`


