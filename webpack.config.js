const path = require('path');
const nodeExternals = require('webpack-node-externals');

module.exports = {
    target: 'node',
  entry: './src/index.ts',
  externals: [nodeExternals()],

  module: {
    rules: [
      {
        use: 'ts-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.node$/,
        use: 'node-loader'
      }
    ],
  },
  resolve: {
    extensions: ['.ts', '.js' ],
  },
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, '.'),
  },
  externals: [nodeExternals()],
};