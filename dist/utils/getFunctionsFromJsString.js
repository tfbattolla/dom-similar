"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Gets functions from js string
 * @param jsString: string
 * @returns string[]
 */
function getFunctionsFromJsString(jsString) {
    return jsString.match(/([. ]*[\w\d]*?)\(/ig);
}
exports.getFunctionsFromJsString = getFunctionsFromJsString;
//# sourceMappingURL=getFunctionsFromJsString.js.map