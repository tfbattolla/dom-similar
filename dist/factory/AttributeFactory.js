"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Attribute_1 = require("../attributes/Attribute");
const IdAttribute_1 = __importDefault(require("../attributes/IdAttribute"));
const ClassAttribute_1 = __importDefault(require("../attributes/ClassAttribute"));
const HrefAttribute_1 = __importDefault(require("../attributes/HrefAttribute"));
const TextContentAttribute_1 = __importDefault(require("../attributes/TextContentAttribute"));
const JsAttribute_1 = __importDefault(require("../attributes/JsAttribute"));
const TitleAttribute_1 = __importDefault(require("../attributes/TitleAttribute"));
const CssPathAttribute_1 = __importDefault(require("../attributes/CssPathAttribute"));
const RefAttribute_1 = __importDefault(require("../attributes/RefAttribute"));
class AttributeFactory {
    static getAttribute(name, data, weight) {
        if (name === 'id') {
            return new IdAttribute_1.default(data, weight);
        }
        else if (name === 'class') {
            return new ClassAttribute_1.default(data, weight);
        }
        else if (name === 'href') {
            return new HrefAttribute_1.default(data, weight);
        }
        else if (name === 'ref') {
            return new RefAttribute_1.default(data, weight);
        }
        else if (name === 'content') {
            return new TextContentAttribute_1.default(data, weight);
        }
        else if (name.startsWith('on')) {
            return new JsAttribute_1.default(name, data, weight);
        }
        else if (name === 'title') {
            return new TitleAttribute_1.default(data, weight);
        }
        else if (name === 'cssPath') {
            return new CssPathAttribute_1.default(data, weight);
        }
        return new Attribute_1.SingleStringAttribute(name, data, weight | 1);
    }
}
exports.default = AttributeFactory;
//# sourceMappingURL=AttributeFactory.js.map