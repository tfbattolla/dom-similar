"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ButtonElement_1 = __importDefault(require("../elements/ButtonElement"));
const ComparableElement_1 = __importDefault(require("../elements/ComparableElement"));
class ElementFactory {
    static getComparableElement(rawElement) {
        switch (rawElement.tagName) {
            case 'A':
                return ButtonElement_1.default.fromRawElement(rawElement);
            case 'BUTTON':
                return ButtonElement_1.default.fromRawElement(rawElement);
            default:
                return ComparableElement_1.default.fromRawElement(rawElement);
        }
    }
}
exports.default = ElementFactory;
//# sourceMappingURL=ElementFactory.js.map