"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const AttributeFactory_1 = __importDefault(require("../factory/AttributeFactory"));
const css_path_1 = __importDefault(require("css-path"));
/**
 * Html Element with attributes to compare with other Comparable Elements. Specifying the weight of each attribute.
 */
class ComparableElement {
    constructor(attributes) {
        this.attributes = attributes;
    }
    /**
     * Compares with an element getting the similarity score
     * @param element
     * @returns score: number
     */
    compareElement(element) {
        const compareElement = ComparableElement.fromRawElement(element);
        return Object.keys(this.attributes).reduce((acc, name) => {
            const attribute = this.attributes[name];
            const compareAttribute = compareElement.attributes[name];
            if (attribute && compareAttribute) {
                return acc + attribute.getSimilarity(compareAttribute);
            }
            return acc;
        }, 0);
    }
    /**
     * Generate Comparable Element from an Html Element
     * @param element
     * @returns ComparableElement
     */
    static fromRawElement(element) {
        const attributesName = element.getAttributeNames();
        const attributes = attributesName.reduce((acc, name) => {
            acc[name] = AttributeFactory_1.default.getAttribute(name, element.getAttribute(name), this.weights[name]);
            return acc;
        }, {});
        attributes['cssPath'] = AttributeFactory_1.default.getAttribute('cssPath', css_path_1.default(element), this.weights['cssPath']);
        attributes['content'] = AttributeFactory_1.default.getAttribute('cssPath', element.textContent, this.weights['content']);
        return new ComparableElement(attributes);
    }
}
exports.default = ComparableElement;
ComparableElement.weights = {
    id: 377,
    'class': 1,
    'cssPath': 1,
    'title': 1,
    'href': 1,
    'jsFunctions': 1,
    'content': 1,
    'ref': 1
};
//# sourceMappingURL=ComparableElement.js.map