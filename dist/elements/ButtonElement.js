"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ComparableElement_1 = __importDefault(require("./ComparableElement"));
class ButtonElement extends ComparableElement_1.default {
}
exports.default = ButtonElement;
ButtonElement.weights = {
    id: 377,
    'class': 144,
    'cssPath': 55,
    'title': 34,
    'href': 34,
    'jsFunctions': 21,
    'content': 144,
    'ref': 5
};
//# sourceMappingURL=ButtonElement.js.map