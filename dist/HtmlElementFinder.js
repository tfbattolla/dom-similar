"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jsdom_1 = require("jsdom");
const fs_1 = __importDefault(require("fs"));
const css_path_1 = __importDefault(require("css-path"));
const ElementFactory_1 = __importDefault(require("./factory/ElementFactory"));
/**
 * Html element finder
 */
class HtmlElementFinder {
    constructor(targetHtmlPath, sampleHtmlPath) {
        this.targetHtmlPath = targetHtmlPath;
        this.sampleHtmlPath = sampleHtmlPath;
    }
    /**
     * Gets document using JSDOM
     * @param filePath
     * @returns
     */
    static getDocument(filePath) {
        const file = fs_1.default.readFileSync(filePath);
        const jsdom = new jsdom_1.JSDOM(file);
        return jsdom.window.document;
    }
    /**
     * Find the element that matchs more accurately the element of the target file
     * @param targetSelector
     * @returns {
     *  element: Element,
     *  score: number
     * }
     */
    findBestElementMatch(targetSelector) {
        const targetRawElement = HtmlElementFinder.getDocument(this.targetHtmlPath).querySelector(targetSelector);
        const targetElement = ElementFactory_1.default.getComparableElement(targetRawElement);
        if (targetElement) {
            const sampleDom = HtmlElementFinder.getDocument(this.sampleHtmlPath);
            const allElements = sampleDom.querySelectorAll('*');
            let bestMatch;
            let biggestNumber = 0;
            allElements.forEach((el) => {
                const num = targetElement.compareElement(el);
                if (num > biggestNumber) {
                    bestMatch = el;
                    biggestNumber = num;
                }
            });
            return { element: bestMatch, score: biggestNumber };
        }
        throw new Error(`Element not found with selector: ${targetSelector}`);
    }
    /**
     * Find the path of the element that matchs more accurately the element of the target file
     * @param targetSelector
     * @returns {
        *  element: Element,
        *  score: number
        * }
        */
    findBestElementMatchPath(targetSelector) {
        const { element, score } = this.findBestElementMatch(targetSelector);
        return { path: css_path_1.default(element), score: score };
    }
}
exports.default = HtmlElementFinder;
//# sourceMappingURL=HtmlElementFinder.js.map