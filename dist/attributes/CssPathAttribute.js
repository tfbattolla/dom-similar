"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Attribute_1 = require("./Attribute");
class CssPathAttribute extends Attribute_1.StringAttribute {
    constructor(data, weight = 100) {
        super('cssPath', data, weight);
    }
    getComparable(data) {
        const path = data.split('>');
        return path;
    }
    getSimilarity(cssPathAttribute) {
        let coincidence = 0;
        const pathLength = this.comparable.length;
        for (let index = 0; index < pathLength; index++) {
            const element = this.comparable[index];
            const otherElement = cssPathAttribute.comparable[index];
            if (element && otherElement && (element === otherElement)) {
                coincidence++;
            }
            else {
                break;
            }
        }
        return coincidence * this.weigth;
    }
}
exports.default = CssPathAttribute;
//# sourceMappingURL=CssPathAttribute.js.map