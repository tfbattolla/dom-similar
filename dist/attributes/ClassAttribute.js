"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Attribute_1 = require("./Attribute");
class ClassAttribute extends Attribute_1.StringAttribute {
    constructor(data, weight = 10) {
        super('class', data, weight);
    }
    getComparable(data) {
        return new Set(data.split(' '));
    }
    getSimilarity(classAttribute) {
        let coincidence = 0;
        classAttribute.comparable.forEach((c) => {
            if (this.comparable.has(c)) {
                coincidence++;
            }
        });
        return coincidence * this.weigth;
    }
}
exports.default = ClassAttribute;
//# sourceMappingURL=ClassAttribute.js.map