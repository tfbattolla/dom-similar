"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Attribute_1 = require("./Attribute");
class HrefAttribute extends Attribute_1.SingleStringAttribute {
    constructor(data, weight = 1000) {
        super('href', data, weight);
    }
}
exports.default = HrefAttribute;
//# sourceMappingURL=HrefAttribute.js.map