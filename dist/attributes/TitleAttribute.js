"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Attribute_1 = require("./Attribute");
class TitleAttribute extends Attribute_1.SingleStringAttribute {
    constructor(data, weight = 1000) {
        super('title', data, weight);
    }
}
exports.default = TitleAttribute;
//# sourceMappingURL=TitleAttribute.js.map