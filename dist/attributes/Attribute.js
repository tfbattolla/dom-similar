"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Attribute {
    constructor(name, data, weigth) {
        this.name = name;
        this.data = data;
        this.weigth = weigth;
        this.comparable = this.getComparable(this.data);
    }
}
exports.Attribute = Attribute;
class StringAttribute extends Attribute {
    constructor(name, data, weight = 1) {
        super(name, data, weight);
    }
}
exports.StringAttribute = StringAttribute;
class SingleStringAttribute extends StringAttribute {
    constructor(name, data, weight = 1) {
        super(name, data, weight);
    }
    getComparable(data) {
        return data.trim();
    }
    getSimilarity(attribute) {
        return attribute.comparable === this.comparable ? this.weigth : 0;
    }
}
exports.SingleStringAttribute = SingleStringAttribute;
//# sourceMappingURL=Attribute.js.map