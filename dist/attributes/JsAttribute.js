"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Attribute_1 = require("./Attribute");
const getFunctionsFromJsString_1 = require("../utils/getFunctionsFromJsString");
class JsAttribute extends Attribute_1.StringAttribute {
    constructor(name, data, weight = 100) {
        super(name, data, weight);
    }
    getComparable(data) {
        const functions = getFunctionsFromJsString_1.getFunctionsFromJsString(data);
        return new Set(functions);
    }
    getSimilarity(classAttribute) {
        let coincidence = 0;
        classAttribute.comparable.forEach((c) => {
            if (this.comparable.has(c)) {
                coincidence++;
            }
        });
        return coincidence * this.weigth;
    }
}
exports.default = JsAttribute;
//# sourceMappingURL=JsAttribute.js.map