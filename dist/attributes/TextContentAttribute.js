"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Attribute_1 = require("./Attribute");
class TextContentAttribute extends Attribute_1.StringAttribute {
    constructor(data, weight = 10) {
        super('text', data, weight);
    }
    getComparable(data) {
        return new Set(data.split(' '));
    }
    getSimilarity(textAttribute) {
        let coincidence = 0;
        textAttribute.comparable.forEach((c) => {
            if (this.comparable.has(c)) {
                coincidence++;
            }
        });
        return coincidence * this.weigth;
    }
}
exports.default = TextContentAttribute;
//# sourceMappingURL=TextContentAttribute.js.map