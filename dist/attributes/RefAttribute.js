"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Attribute_1 = require("./Attribute");
class RefAttribute extends Attribute_1.SingleStringAttribute {
    constructor(data, weight = 100) {
        super('ref', data, weight);
    }
}
exports.default = RefAttribute;
//# sourceMappingURL=RefAttribute.js.map