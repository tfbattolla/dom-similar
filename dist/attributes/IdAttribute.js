"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Attribute_1 = require("./Attribute");
class IdAttribute extends Attribute_1.SingleStringAttribute {
    constructor(data, weight = 1000) {
        super('id', data, weight);
    }
}
exports.default = IdAttribute;
//# sourceMappingURL=IdAttribute.js.map